//! Subplot test utilities
//!
//! This crate is meant to be used by test suites generated using the [Subplot][]
//! code generator using the `rust` template.  You should generally not use this
//! crate directly, instead test suites generated using the `rust` template will
//! `use subplotlib::prelude::*;` automatically.
//!
//! If you want to explore things, then we suggest you start from the [`prelude`][mod@prelude]
//! module since it exposes everything you'd normally find in a test suite.
//!
//! [Subplot]: https://subplot.liw.fi/

pub mod file;
pub mod prelude;
pub mod scenario;
pub mod step;
pub mod steplibrary;
pub mod types;
pub mod utils;

/// Generate subplot scenario code from a given Subplot input
///
/// The first, and only mandatory, argument to this is a string representing the
/// path to the subplot file to load, relative to the `CARGO_MANIFEST_DIR` of
/// the crate being built.
///
/// So, if you have `Cargo.toml` and `myprog.subplot` at the same level,
/// you can do `codegen!("myprog.subplot")`
///
/// If you want to limit the generated scenarios, for example if you want
/// to exclude scenarios labelled with `slow`, or only include scenarios
/// labelled `critical` then you can use the `include` and `exclude` named
/// arguments.  For example `codegen!("foo.subplot", include="critical")`
/// or `codegen!("foo.subplot", exclude="slow")`
///
/// The include/exclude behaviour, and filter syntax, matches that used by
/// the `subplot codegen` subcommand from `subplot-bin`.
pub use subplotlib_derive::codegen;
