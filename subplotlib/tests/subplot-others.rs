#[cfg(inside_subplot = "yes")]
subplotlib::codegen!(
    "../subplot.subplot",
    exclude = "codegen, diagrams, docgen, extract, metadata"
);
