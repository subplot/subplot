//! Subplot seq example build script.

fn main() {
    println!("cargo:rerun-if-changed=seq.subplot");
    subplot_build::codegen("seq.subplot").expect("failed to generate code with Subplot");
}
