use codegen::do_codegen;
use proc_macro::TokenStream;
use syn::parse_macro_input;

mod codegen;
mod step;

#[proc_macro_attribute]
pub fn step(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as step::StepFn);

    if let Err(e) = step::check_step_declaration(&input) {
        return e.to_compile_error().into();
    }

    match step::process_step(input) {
        Ok(toks) => toks.into(),
        Err(e) => e.to_compile_error().into(),
    }
}

#[proc_macro]
pub fn codegen(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as codegen::CodegenArgs);

    match do_codegen(input) {
        Ok(toks) => toks.into(),
        Err(e) => e.to_compile_error().into(),
    }
}
