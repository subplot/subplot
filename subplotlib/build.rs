// Build script for subplotlib.
//
// All we want is to check if we're inside subplot's source.  If we are
// then we can test everything, otherwise just subplotlib.subplot.

fn main() {
    // Because we cannot generate tests if we're not fully inside the main subplot tree
    // we only generate them if we can see ../subplot.subplot which is a good indicator.
    println!("cargo::rustc-check-cfg=cfg(inside_subplot, values(\"yes\"))");
    if std::fs::metadata("../subplot.subplot").is_ok() {
        println!("cargo::rustc-cfg=inside_subplot=\"yes\"");
    }
}
