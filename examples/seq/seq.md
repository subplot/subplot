---
title: "**seq**(1) acceptance tests"
author: The Subplot project
bindings:
  - lib/runcmd.yaml
  - seq-extras.yaml
impls:
  rust:
    - seq-extras.rs
...

# Introduction

**seq**(1) is a Unix command line tool that produces numbers 1, 2,
etc, up until a count given by the user. For example, if the user runs
`seq 3`, the output has the numbers 1, 2, and 3, one per line.

# No arguments

_Requirement: If `seq` is run without arguments, it reports an error._

```scenario
when I try to run seq
then exit code is 1
```

# No numbers

_Requirement: If `seq` is run with the argument 0, it writes nothing._

```scenario
when I run seq 0
then stdout is exactly ""
then stderr is exactly ""
```

# One number

_Requirement: If `seq` is run with the argument 1, it writes one line,
with the number 1._

```scenario
when I run seq 1
then stdout is exactly "1\n"
then stderr is exactly ""
```

# More than one number

_Requirement: If `seq` is run with the argument 3, it writes three lines,
one with the number 1, one with 2, and one with 3._

```scenario
when I run seq 3
then stdout has 3 lines
then stdout is exactly "1\n2\n3\n"
then stderr is exactly ""
```

# Not a number

_Requirement: If `seq` is run with a non-numerical argument, it writes nothing
to stdout, and an error message to stderr._

```scenario
when I try to run seq fish
then command fails
then stdout is exactly ""
then stderr says fish is invalid
```
