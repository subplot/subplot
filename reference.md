# Introduction

This document describes how we guard against accidental breaking
changes in Subplot by running the current version against a curated
set of subplot documents.

# A Subplot version from a little before 0.12

## Produce HTML page

~~~scenario
given an installed subplot
given a clone of https://gitlab.com/subplot/subplot.git in src at 73eb9c894f47aa16b414c86050cbda83bf3ce2c8
when I docgen subplot.subplot to test.html, in src
when I run, in src, subplot docgen subplot.subplot --merciful -o subplot.html -t python
then file src/test.html exists
~~~

## Generate and run test program

~~~scenario
given an installed subplot
given file run_test.sh
given a clone of https://gitlab.com/subplot/subplot.git in src at 73eb9c894f47aa16b414c86050cbda83bf3ce2c8
when I run, in src, subplot codegen subplot.subplot -o test-inner.py -t python
when I run bash run_test.sh
then command is successful
~~~

~~~{#run_test.sh .file .sh}
#!/bin/bash

set -euo pipefail

N=100

if python3 src/test-inner.py --log test-inner.log --env "PATH=$PATH" --env SUBPLOT_DIR=/
then
    exit=0
else
    exit="$?"
fi

if [ "$exit" != 0 ]
then
    # Failure. Show end of inner log file.

    echo "last $N lines of test-inner.log:"
    tail "-n$N" test-inner.log | sed 's/^/    /'
fi

exit "$exit"
~~~
